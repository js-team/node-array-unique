# Installation
> `npm install --save @types/array-unique`

# Summary
This package contains type definitions for array-unique (https://github.com/jonschlinkert/array-unique).

# Details
Files were exported from https://www.github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/array-unique

Additional Details
 * Last updated: Thu, 15 Mar 2018 23:17:55 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by Michaël St-Georges <https://github.com/CSLTech>.
